﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace sLibV1
{
    public class Validar
    {
        public static bool ValidarOpcionMenu(string rgx, dynamic op)
        {
            var m = Regex.Match(Convert.ToString(op), rgx);
            var r = m.Success;
            return r;
        }

        public static string ValidarOpcionMenuString(string rgx, dynamic op)
        {
            var m = Regex.Match(Convert.ToString(op), rgx);
            var r = m.Value;
            return r;
        }

        public static string ValidarPalabra(string validar)
        {
            try
            {
                validar = Console.ReadLine().Trim();
                if (validar.Length == 0)
                {
                    Mensajes.MensajeError("Campo vacio!");
                    Console.ReadKey();
                }
                else if (validar.Length <= 2)
                {
                    Mensajes.MensajeError("Nombre muy corto!");
                    Console.ReadKey();
                }
                else if (validar.Length >= 30)
                {
                    Mensajes.MensajeError("Nombre muy largo!");
                    Console.ReadKey();
                }
                else if (Validar.ValidarOpcionMenu(@"([^A-Za-z\s_]+)", validar))
                {
                    Mensajes.MensajeError("Nombre invalido!\n- No utilice numeros\n- No utilice caracteres especiales\n- No utilice nombres muy largos o cortos");
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Mensajes.MensajeError("Nombre invalido!\n- No utilice numeros\n- No utilice caracteres especiales\n- No utilice nombres muy largos o cortos\n\n" + e);
                Console.ReadKey();
                validar = "";
            }
            return validar;
        }

        public static string ValidarPalabra(string validar, int parametro)
        {
            switch (parametro)
            {
                case 0:
                    //permite palabras, espacios y los siguientes caracteres: ,._/-
                    try
                    {
                        validar = Console.ReadLine().Trim();
                        if (validar.Length == 0)
                        {
                            Mensajes.MensajeError("Campo vacio!");
                            Console.ReadKey();
                        }
                        else if (validar.Length <= 2)
                        {
                            Mensajes.MensajeError("Nombre muy corto!");
                            Console.ReadKey();
                        }
                        else if (validar.Length >= 30)
                        {
                            Mensajes.MensajeError("Nombre muy largo!");
                            Console.ReadKey();
                        }
                        else if (Validar.ValidarOpcionMenu(@"([^A-Za-z0-9\s,._/-]+)", validar))
                        {
                            Mensajes.MensajeError("Nombre invalido!\n- No utilice numeros\n- No utilice caracteres especiales\n- No utilice nombres muy largos o cortos");
                            Console.ReadKey();
                        }
                    }
                    catch (Exception e)
                    {
                        Mensajes.MensajeError(
                            "Nombre invalido!\n- No utilice numeros\n- No utilice caracteres especiales\n- No utilice nombres muy largos o cortos\n\n" +
                            e);
                        Console.ReadKey();
                        return validar = "";
                    }
                    return validar;
                default:
                    return validar;
            }
        }

        public static bool ValidarPalabraWhile(string validar)
        {
            return ValidarOpcionMenu(@"([^A-Za-z\s_]+)", validar) || validar.Length == 0 || validar.Length <= 2 || validar.Length >= 30;
        }

        public static bool ValidarPalabraWhile(string validar, int parametro)
        {
            bool r = true;
            switch (parametro)
            {
                case 0:
                    //permite palabras, espacios y los siguientes caracteres: ,._/-
                    r = ValidarOpcionMenu(@"([^A-Za-z0-9\s,._/-]+)", validar) || validar.Length == 0 || validar.Length <= 2 || validar.Length >= 30;
                    break;
                default:
                    Mensajes.MensajeError("Parametro de metodo 'ValidarPalabraWhile' o 'ValidarPalabra' invalido ");
                    Console.WriteLine("\nVolviendo al menú principal...");
                    Console.ReadKey();
                    //MenuPrincipal();
                    r = true;
                    break;
            }
            return r;
        }
    }

    public class Estilo
    {
        public static string Encabezado(string e)
        {
            int lg = Console.WindowWidth;
            //if (lg > 80) { lg = 80; }
            if (lg % 2 == 1) { lg++; }

            e = e.ToUpper();
            var lgE = e.Length;

            if (lgE % 2 == 1) { e += " "; lgE = e.Length; }

            string l1 = null;

            for (var i = 0; i < lg; i++) { l1 += "-"; }
            if (lg < 80) { l1 += "\n|"; } else { l1 += "|"; }
            for (var i = 1; i < (lg / 2) - (lgE / 2); i++) { l1 += " "; }
            l1 += e;
            for (var i = 1; i < (lg / 2) - (lgE / 2); i++) { l1 += " "; }
            if (lg < 80) { l1 += "|\n"; } else { l1 += "|"; }
            for (var i = 0; i < lg; i++) { l1 += "-"; }

            var r = l1;
            return r;
        }

        public static void Paginar(string encabezado, string[] arr, int itemsPorPagina)
        {
            int totalItems = arr.Length - 1;
            int totalPaginas = 0;

            totalPaginas = totalItems / itemsPorPagina;
            if (totalItems % itemsPorPagina > 0) { totalPaginas++; }

            int i, j;

            for (int k = 1; k <= totalPaginas; k++)
            {
                Console.Clear();
                Encabezado(encabezado);

                string msg = CentrarTexto("Mostrando pagina {0} de {1}");

                if (totalItems > itemsPorPagina)
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(msg, k, totalPaginas);
                    Console.ResetColor();
                    /*Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;*/
                }

                i = (k - 1) * itemsPorPagina;
                j = k * itemsPorPagina;
                int contador = 1;

                string relleno = null;
                for (int l = 0; l < Console.WindowWidth; l++) { relleno += "-"; }

                for (; i < j; i++)
                {
                    if (i >= totalItems) { break; }
                    Console.Write((contador == itemsPorPagina || i == totalItems) ? arr[i] + "\n\n" : arr[i] + "\n" + relleno);
                    contador++;
                    if (contador > itemsPorPagina) { contador = 1; }
                }

                if (totalItems > itemsPorPagina)
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(msg, k, totalPaginas);
                    Console.ResetColor();
                    /*Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;*/
                }

                if (i != totalItems)
                {
                    Console.WriteLine("\nPresione cualquier tecla para continuar...");
                    Console.ReadLine();
                }
            }
        }

        public static void Paginar(string encabezado, List<string> arr, int itemsPorPagina)
        {
            int totalItems = arr.Count;
            int totalPaginas = 0;

            totalPaginas = totalItems / itemsPorPagina;
            if (totalItems % itemsPorPagina > 0) { totalPaginas++; }

            int i, j;

            for (int k = 1; k <= totalPaginas; k++)
            {
                Console.Clear();
                Encabezado(encabezado);

                string msg = CentrarTexto("Mostrando pagina {0} de {1}");

                if (totalItems > itemsPorPagina)
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(msg, k, totalPaginas);
                    Console.ResetColor();
                    /*Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;*/
                }

                i = (k - 1) * itemsPorPagina;
                j = k * itemsPorPagina;
                int contador = 1;

                string relleno = null;
                for (int l = 0; l < Console.WindowWidth; l++) { relleno += "-"; }

                for (; i < j; i++)
                {
                    if (i >= totalItems) { break; }
                    Console.Write((contador == itemsPorPagina || i == (totalItems - 1)) ? arr[i] + "\n\n" : arr[i] + "\n" + relleno);
                    contador++;
                    if (contador > itemsPorPagina) { contador = 1; }
                }

                if (totalItems > itemsPorPagina)
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(msg, k, totalPaginas);
                    Console.ResetColor();
                    /*Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;*/
                }

                if (i != totalItems)
                {
                    Console.WriteLine("\nPresione cualquier tecla para continuar...");
                    Console.ReadLine();
                }
            }
        }

        public static void Paginar(string encabezado, List<string> informacion, List<string> arr, int itemsPorPagina)
        {
            int totalItems = arr.Count;
            int totalPaginas = 0;

            totalPaginas = totalItems / itemsPorPagina;
            if (totalItems % itemsPorPagina > 0) { totalPaginas++; }

            int i, j;

            for (int k = 1; k <= totalPaginas; k++)
            {
                Console.Clear();
                Encabezado(encabezado);

                foreach (var item in informacion) { Console.WriteLine(item); }

                string msg = CentrarTexto("Mostrando pagina {0} de {1}");
                if (totalItems > itemsPorPagina)
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(msg, k, totalPaginas);
                    Console.ResetColor();
                    /*Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;*/
                }

                i = (k - 1) * itemsPorPagina;
                j = k * itemsPorPagina;
                int contador = 1;

                string relleno = null;
                for (int l = 0; l < Console.WindowWidth; l++) { relleno += "-"; }

                for (; i < j; i++)
                {
                    if (i >= totalItems) { break; }
                    Console.Write((contador == itemsPorPagina || i == (totalItems - 1)) ? arr[i] + "\n\n" : arr[i] + "\n" + relleno);
                    contador++;
                    if (contador > itemsPorPagina) { contador = 1; }
                }

                if (totalItems > itemsPorPagina)
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(msg, k, totalPaginas);
                    Console.ResetColor();
                    /*Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;*/
                }

                if (i != totalItems)
                {
                    Console.WriteLine("\nPresione cualquier tecla para continuar...");
                    Console.ReadLine();
                }
            }
        }

        public static string CentrarTexto(string e)
        {
            int lg = Console.WindowWidth;
            if (e == "Mostrando pagina {0} de {1}") { lg += 5; }

            if (lg % 2 == 1) { lg++; }
            var lgE = e.Length;
            if (lgE % 2 == 1) { e += " "; lgE = e.Length; }
            string l1 = null;
            for (var i = 1; i < (lg / 2) - (lgE / 2); i++) { l1 += " "; }
            l1 += e;
            for (var i = 1; i < (lg / 2) - (lgE / 2); i++) { l1 += " "; }
            var r = l1;
            return r;
        }

        public static string CentrarTexto(string e, int offset)
        {
            int lg = Console.WindowWidth;
            if (e == "Mostrando pagina {0} de {1}") { lg += 5; }

            lg += offset;

            if (lg % 2 == 1) { lg++; }
            var lgE = e.Length;
            if (lgE % 2 == 1) { e += " "; lgE = e.Length; }
            string l1 = null;
            for (var i = 1; i < (lg / 2) - (lgE / 2); i++) { l1 += " "; }
            l1 += e;
            for (var i = 1; i < (lg / 2) - (lgE / 2); i++) { l1 += " "; }
            var r = l1;
            return r;
        }
    }

    public class Mensajes
    {
        public static void MensajeError(string msg, int a, int b)
        {
            if (msg == "seleccioneEntre") { msg = "Seleccione entre {0} y {1}!"; }

            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\nERROR: " + msg, a, b);
            Console.ResetColor();
            /*Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.DarkCyan;*/
        }

        public static void MensajeError(string msg)
        {
            if (msg == "soloNumeros") { msg = "Solo se permiten numeros!"; }
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\nERROR: " + msg);
            Console.ResetColor();
            /*Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.DarkCyan;*/
        }

        public static void MensajeAlerta(string msg)
        {
            if (msg == "noCategorias") { msg = "No existen categorias!"; }
            Console.BackgroundColor = ConsoleColor.DarkYellow;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\nALERTA: " + msg);
            Console.ResetColor();
            /*Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.DarkCyan;*/
        }

        public static void MensajeValido(string msg)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n" + msg);
            Console.ResetColor();
            /*Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.DarkCyan;*/
        } 
    }

    public class Prueba
    {
        public static void GenerarError()
        {
            object o2 = null;
            int i2 = (int)o2;
        }
    }
}
